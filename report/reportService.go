package report

type ReportServiceAPI interface {
	GWLogIn()
	GWLogOut()
	NodesLogIn()
	NodesLogOut()
	GWPropertyReport()
	NodesPropertyReport()
}

func init() {

}

func ReportServiceInit() {

}
